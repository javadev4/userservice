package com.nilcompany.userservice.service;

import com.nilcompany.userservice.entity.User;

public interface UserService {
	public User getUser(Long id);
}
