package com.nilcompany.userservice.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.nilcompany.userservice.entity.Contact;
import com.nilcompany.userservice.entity.User;
import com.nilcompany.userservice.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/users")
public class UserController {
	private final UserService userService;
	private final RestTemplate restTemplate;

	public UserController(UserService userService, RestTemplate restTemplate) {
		super();
		this.userService = userService;
		this.restTemplate = restTemplate;
	}

	@GetMapping("/{userId}")
	public User getUser(@PathVariable Long userId) {
		log.info("getUser : " + userId);
		User user = userService.getUser(userId);
		String link = "http://contact-service/contacts/users/" + user.getUserId();
		log.info("Link " + link);
		List<Contact> contactList = this.restTemplate.getForObject(link, List.class);
		user.setContacts(contactList);
		return user;
	}
}
