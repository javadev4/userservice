package com.nilcompany.userservice.dto;

import java.util.ArrayList;
import java.util.List;

import com.nilcompany.userservice.entity.Contact;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ContactList {
    private List<Contact> contact;
    
    public ContactList() {
    	contact = new ArrayList<>();
    }
}
