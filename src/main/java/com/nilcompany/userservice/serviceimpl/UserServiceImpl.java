package com.nilcompany.userservice.serviceimpl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.nilcompany.userservice.entity.User;
import com.nilcompany.userservice.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

	List<User> users = List.of(new User(1311L, "Nil", "15213121405"), new User(1312L, "Jack Ma", "15213121406"),
			new User(1313L, "Peter", "15213121457"));

	@Override
	public User getUser(Long id) {
		log.info("getUser : " + id);
		return users.stream().filter(user -> user.getUserId().equals(id)).findAny().orElse(null);
	}

}
